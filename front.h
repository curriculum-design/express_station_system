#pragma once
#include <iostream>
#include "dataOperation.h"

class front
{
public:
	static void displayAllPackage()
	{
		for (package &p : dataOperation::allPackage)
			cout << p.display() << endl;
	}

	static package packageInStorage()
	{
		cout << "请输入快递单号，寄件人姓名，收件人姓名，包裹名稱,重量，体积及价钱" << endl;
		string courierNumber;
		string consignor;
		string consignee;
		string name;
		float weight;
		float volume;
		float value;
		cin >> courierNumber >> consignor >> consignee >> name >> weight >> volume >> value;
		package p(courierNumber, consignor, consignee, name, weight, volume, value, *dataOperation::systemTime);
		dataOperation::allPackage.push_back(p);
		return p;
	}
	static express ExpressMail()
	{
		cout << "请输入寄件人姓名，收件人姓名，所寄物品,重量，体积及价钱" << endl;
		string courierNumber;
		string consignor;
		string consignee;
		string name;
		float weight;
		float volume;
		float value;
		cin >> consignor >> consignee >> name >> weight >> volume >> value;
		express e(consignor, consignee, name, weight, volume, value, *dataOperation::systemTime);
		dataOperation::allExpress.push_back(e);
		return e;
	}
};
void cls();